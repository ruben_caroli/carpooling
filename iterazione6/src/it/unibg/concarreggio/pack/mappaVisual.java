package it.unibg.concarreggio.pack;

import it.unibg.concarreggio.pack.Controller;

import java.awt.Dimension;

import javax.swing.JFrame;

import org.jgrapht.Graph;
import org.jgrapht.experimental.touchgraph.TouchgraphPanel;
import org.jgrapht.graph.DefaultWeightedEdge;


public class mappaVisual {

	public static void map(){
		Graph<String, DefaultWeightedEdge> g = Controller.creaMappa();
	    boolean selfReferencesAllowed = false;
	    JFrame frame = new JFrame();
	    frame.getContentPane().add(
	        new TouchgraphPanel<String, DefaultWeightedEdge>(g, selfReferencesAllowed));
	    frame.setPreferredSize(new Dimension(600, 400));
	    frame.setTitle("JGraphT to Touchgraph Converter Demo");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.pack();
	    frame.setVisible(true);
	    try {
	        Thread.sleep(5000000);
	    } catch (InterruptedException ex) {
	    }
	}
	public mappaVisual(){
		Graph<String, DefaultWeightedEdge> g = Controller.creaMappa();
	    boolean selfReferencesAllowed = false;
	    JFrame frame = new JFrame();
	    frame.getContentPane().add(
	        new TouchgraphPanel<String, DefaultWeightedEdge>(g, selfReferencesAllowed));
	    frame.setPreferredSize(new Dimension(600, 400));
	    frame.setTitle("JGraphT to Touchgraph Converter Demo");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.pack();
	    frame.setVisible(true);
	    try {
	        Thread.sleep(5000000);
	    } catch (InterruptedException ex) {
	    }
	}
	
	public static void main(String[] args) {
		map();

}
}
