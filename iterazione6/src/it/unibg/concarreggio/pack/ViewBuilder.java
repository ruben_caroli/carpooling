package it.unibg.concarreggio.pack;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.JScrollBar;
import java.awt.Font;


public class ViewBuilder {

	private JFrame frame;
	private JTextField altroCampo;
	private JTextField percUtente;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewBuilder window = new ViewBuilder();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewBuilder() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 889, 552);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblUtente = new JLabel("Codice utente:");
		lblUtente.setForeground(Color.BLUE);
		lblUtente.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblUtente = new GridBagConstraints();
		gbc_lblUtente.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtente.gridx = 4;
		gbc_lblUtente.gridy = 1;
		frame.getContentPane().add(lblUtente, gbc_lblUtente);
		
		JLabel lblNumeroPercorso = new JLabel("Numero percorso:");
		lblNumeroPercorso.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblNumeroPercorso = new GridBagConstraints();
		gbc_lblNumeroPercorso.insets = new Insets(0, 0, 5, 5);
		gbc_lblNumeroPercorso.gridx = 6;
		gbc_lblNumeroPercorso.gridy = 1;
		frame.getContentPane().add(lblNumeroPercorso, gbc_lblNumeroPercorso);
		
		JLabel lblCittPartenza = new JLabel("Citt\u00E0 partenza:");
		lblCittPartenza.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblCittPartenza = new GridBagConstraints();
		gbc_lblCittPartenza.insets = new Insets(0, 0, 5, 5);
		gbc_lblCittPartenza.anchor = GridBagConstraints.EAST;
		gbc_lblCittPartenza.gridx = 0;
		gbc_lblCittPartenza.gridy = 2;
		frame.getContentPane().add(lblCittPartenza, gbc_lblCittPartenza);
		
		final JComboBox partenza1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_partenza1 = new GridBagConstraints();
		gbc_partenza1.insets = new Insets(0, 0, 5, 5);
		gbc_partenza1.fill = GridBagConstraints.HORIZONTAL;
		gbc_partenza1.gridx = 1;
		gbc_partenza1.gridy = 2;
		frame.getContentPane().add(partenza1, gbc_partenza1);
		
		altroCampo = new JTextField();
		altroCampo.setToolTipText("inserire il codice univoco (codice fiscale) dell'utente");
		GridBagConstraints gbc_altroCampo = new GridBagConstraints();
		gbc_altroCampo.insets = new Insets(0, 0, 5, 5);
		gbc_altroCampo.fill = GridBagConstraints.HORIZONTAL;
		gbc_altroCampo.gridx = 4;
		gbc_altroCampo.gridy = 2;
		frame.getContentPane().add(altroCampo, gbc_altroCampo);
		altroCampo.setColumns(10);
		
		percUtente = new JTextField();
		percUtente.setToolTipText("inserire il numero progressivo del percorso di un utente che si vuole visualizzare");
		GridBagConstraints gbc_percUtente = new GridBagConstraints();
		gbc_percUtente.insets = new Insets(0, 0, 5, 5);
		gbc_percUtente.fill = GridBagConstraints.HORIZONTAL;
		gbc_percUtente.gridx = 6;
		gbc_percUtente.gridy = 2;
		frame.getContentPane().add(percUtente, gbc_percUtente);
		percUtente.setColumns(10);
		
		JLabel lblCittArrivo = new JLabel("Citt\u00E0 arrivo:");
		lblCittArrivo.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblCittArrivo = new GridBagConstraints();
		gbc_lblCittArrivo.insets = new Insets(0, 0, 5, 5);
		gbc_lblCittArrivo.anchor = GridBagConstraints.EAST;
		gbc_lblCittArrivo.gridx = 0;
		gbc_lblCittArrivo.gridy = 3;
		frame.getContentPane().add(lblCittArrivo, gbc_lblCittArrivo);
		
		final JComboBox arrivo1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_arrivo1 = new GridBagConstraints();
		gbc_arrivo1.insets = new Insets(0, 0, 5, 5);
		gbc_arrivo1.fill = GridBagConstraints.HORIZONTAL;
		gbc_arrivo1.gridx = 1;
		gbc_arrivo1.gridy = 3;
		frame.getContentPane().add(arrivo1, gbc_arrivo1);
		
		JLabel lblOrarioPartenza = new JLabel("Orario partenza:");
		lblOrarioPartenza.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblOrarioPartenza = new GridBagConstraints();
		gbc_lblOrarioPartenza.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrarioPartenza.anchor = GridBagConstraints.EAST;
		gbc_lblOrarioPartenza.gridx = 0;
		gbc_lblOrarioPartenza.gridy = 4;
		frame.getContentPane().add(lblOrarioPartenza, gbc_lblOrarioPartenza);
		
		ArrayList<Integer> orari = new ArrayList<Integer>();
		for(int i=0;i<24;i++)orari.add(i);

		ArrayList<Integer> minuti = new ArrayList<Integer>();
		for(int i=0;i<60;i+=5)minuti.add(i);
		
		ArrayList<Integer> tolleranze = new ArrayList<Integer>();
		for(int i=0;i<30;i+=1)tolleranze.add(i);
		
		final JComboBox ora1 = new JComboBox(orari.toArray());
		ora1.setToolTipText("ore");
		GridBagConstraints gbc_ora1 = new GridBagConstraints();
		gbc_ora1.insets = new Insets(0, 0, 5, 5);
		gbc_ora1.fill = GridBagConstraints.HORIZONTAL;
		gbc_ora1.gridx = 1;
		gbc_ora1.gridy = 4;
		frame.getContentPane().add(ora1, gbc_ora1);
		
		final JComboBox minuti1 = new JComboBox(minuti.toArray());
		minuti1.setToolTipText("minuti");
		GridBagConstraints gbc_minuti1 = new GridBagConstraints();
		gbc_minuti1.insets = new Insets(0, 0, 5, 5);
		gbc_minuti1.fill = GridBagConstraints.HORIZONTAL;
		gbc_minuti1.gridx = 2;
		gbc_minuti1.gridy = 4;
		frame.getContentPane().add(minuti1, gbc_minuti1);
		

		JLabel lblTolleranza = new JLabel("Tolleranza:");
		lblTolleranza.setForeground(Color.BLUE);
		GridBagConstraints gbc_lblTolleranza = new GridBagConstraints();
		gbc_lblTolleranza.insets = new Insets(0, 0, 5, 5);
		gbc_lblTolleranza.anchor = GridBagConstraints.EAST;
		gbc_lblTolleranza.gridx = 0;
		gbc_lblTolleranza.gridy = 5;
		frame.getContentPane().add(lblTolleranza, gbc_lblTolleranza);
		
		final JComboBox toll1 = new JComboBox(tolleranze.toArray());
		toll1.setToolTipText("tempo che \u00E8 possibile aggiungere o togliere all'orario di partenza");
		GridBagConstraints gbc_toll1 = new GridBagConstraints();
		gbc_toll1.insets = new Insets(0, 0, 5, 5);
		gbc_toll1.fill = GridBagConstraints.HORIZONTAL;
		gbc_toll1.gridx = 1;
		gbc_toll1.gridy = 5;
		frame.getContentPane().add(toll1, gbc_toll1);
		
		JLabel lblMin = new JLabel("min");
		GridBagConstraints gbc_lblMin = new GridBagConstraints();
		gbc_lblMin.insets = new Insets(0, 0, 5, 5);
		gbc_lblMin.gridx = 2;
		gbc_lblMin.gridy = 5;
		frame.getContentPane().add(lblMin, gbc_lblMin);
		

		
		JScrollPane scrollPane = new JScrollPane();
		GridBagConstraints gbc_scrollPane = new GridBagConstraints();
		gbc_scrollPane.gridheight = 2;
		gbc_scrollPane.gridwidth = 6;
		gbc_scrollPane.insets = new Insets(0, 0, 0, 5);
		gbc_scrollPane.fill = GridBagConstraints.BOTH;
		gbc_scrollPane.gridx = 1;
		gbc_scrollPane.gridy = 9;
		frame.getContentPane().add(scrollPane, gbc_scrollPane);
		final JTextArea text1;
		text1 = new JTextArea();
		text1.setFont(new Font("Arial", Font.PLAIN, 16));
		text1.setColumns(10);
		scrollPane.setViewportView(text1);
		text1.setEditable(false);
		text1.setWrapStyleWord(true);
		text1.setLineWrap(true);
		
		JButton btnPercorsoSpecifico = new JButton("Percorso specifico");
		btnPercorsoSpecifico.setToolTipText("inserisci codice utente e numero percorso per visualizzarlo");
		btnPercorsoSpecifico.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user=altroCampo.getText();
				String percorso = percUtente.getText();
				if(user.compareTo("")==0)text1.setText("Inserisci l'utente.");
				else if(percorso.compareTo("")==0)text1.setText("Inserisci il percorso.");
				else{
					int percorsoInt = Integer.parseInt(percorso);
					ArrayList<String> visual = Model.archiPercorso(user, percorsoInt);
					text1.setText("Utente: "+user+", percorso numero "+percorso+": ");
					for(int i=0;i<visual.size();i++){
						text1.append(visual.get(i));	
					}
					
				}
			}
		});
		GridBagConstraints gbc_btnPercorsoSpecifico = new GridBagConstraints();
		gbc_btnPercorsoSpecifico.insets = new Insets(0, 0, 5, 5);
		gbc_btnPercorsoSpecifico.gridx = 6;
		gbc_btnPercorsoSpecifico.gridy = 4;
		frame.getContentPane().add(btnPercorsoSpecifico, gbc_btnPercorsoSpecifico);

		JButton btnNumeroPercorsiUtente = new JButton("Numero percorsi utente");
		btnNumeroPercorsiUtente.setToolTipText("inserisci il codice utente per sapere quanti suoi percorsi sono presenti in archivio");
		btnNumeroPercorsiUtente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user=altroCampo.getText();
				if(user.compareTo("")==0)text1.setText("Inserisci l'utente.");
				else{
				int perc=Model.contaPercorsi(user);
				text1.setText("");
				if(perc==0)text1.append("Utente non presente nell'archivio.");
				else text1.append("Numero percorsi: "+perc);
				}
			}
		});
		GridBagConstraints gbc_btnNumeroPercorsiUtente = new GridBagConstraints();
		gbc_btnNumeroPercorsiUtente.insets = new Insets(0, 0, 5, 5);
		gbc_btnNumeroPercorsiUtente.gridx = 4;
		gbc_btnNumeroPercorsiUtente.gridy = 4;
		
		frame.getContentPane().add(btnNumeroPercorsiUtente, gbc_btnNumeroPercorsiUtente);

		
		JButton btnPercorsiDiUn = new JButton("Percorsi di un utente");
		btnPercorsiDiUn.setToolTipText("inserisci il codice utente per visualizzare tutti i suoi percorsi presenti in archivio");
		btnPercorsiDiUn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String user=altroCampo.getText();
				ArrayList<String> percorsi = Model.percorsiUtente(user);
				if(user.compareTo("")==0)text1.setText("Inserisci l'utente.");
				else
					{
					text1.setText("Percorsi dell'utente "+user+" :\n");
					for(int i=0;i<percorsi.size();i++)text1.append(percorsi.get(i));
					}
				}
		});
		GridBagConstraints gbc_btnPercorsiDiUn = new GridBagConstraints();
		gbc_btnPercorsiDiUn.insets = new Insets(0, 0, 5, 5);
		gbc_btnPercorsiDiUn.gridx = 4;
		gbc_btnPercorsiDiUn.gridy = 5;
		frame.getContentPane().add(btnPercorsiDiUn, gbc_btnPercorsiDiUn);
		
		JButton btnAggiungiPercorsoIn = new JButton("Aggiungi percorso in archivio");
		btnAggiungiPercorsoIn.setToolTipText("inserici codice utente, citt\u00E0, orario partenza per aggiungere il percorso in archivio");
		btnAggiungiPercorsoIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String a = (String)partenza1.getSelectedItem();
				String b = (String)arrivo1.getSelectedItem();

				//le ore e i minuti vengono convertiti in minuti per darli in pasto alla funzione passaggiMinimi
				int r=((Integer) ora1.getSelectedItem()*60)+(Integer) minuti1.getSelectedItem();
				int t=(Integer)toll1.getSelectedItem();
				if(a.compareTo(b)==0)text1.setText("Inserisci due citt� diverse!");
				else{
				String user= altroCampo.getText();
				if(user.compareTo("")==0)text1.setText("Inserisci il codice utente.");
				else{
				int percorsi = Model.insertXML(user, a, b, r);
				if(percorsi==0)percorsi=1;
				text1.setText("Numero di percorsi totali di questo utente: "+percorsi);
				}
				}
			}
		});
		GridBagConstraints gbc_btnAggiungiPercorsoIn = new GridBagConstraints();
		gbc_btnAggiungiPercorsoIn.insets = new Insets(0, 0, 5, 5);
		gbc_btnAggiungiPercorsoIn.gridx = 6;
		gbc_btnAggiungiPercorsoIn.gridy = 8;
		frame.getContentPane().add(btnAggiungiPercorsoIn, gbc_btnAggiungiPercorsoIn);
		
		
		JButton btnRicercaPercorsi = new JButton("Ricerca percorsi simili");
		btnRicercaPercorsi.setToolTipText("inserisci codice utente, citt\u00E0, orario e tolleranza per cercare in archivio percorsi simili");
		btnRicercaPercorsi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a = (String)partenza1.getSelectedItem();
				String b = (String)arrivo1.getSelectedItem();

				//le ore e i minuti vengono convertiti in minuti per darli in pasto alla funzione passaggiMinimi
				int e=((Integer) ora1.getSelectedItem()*60)+(Integer) minuti1.getSelectedItem();
				int f=(Integer)toll1.getSelectedItem();
				
				if(a.compareTo(b)==0)text1.append("Scegli due citt� diverse fra di loro.");
				else{
				text1.setText("");
				String user= altroCampo.getText();
				if(user.compareTo("")==0)text1.append("Inserisci l'utente.");
				else{
					ArrayList<String> ritorno = Model.ricercaXML(user, a, b, e, f);
					for(int l=0;l<ritorno.size();l++)text1.append(ritorno.get(l));
					}
				}
			}
		});
		
		
		GridBagConstraints gbc_btnRicercaPercorsi = new GridBagConstraints();
		gbc_btnRicercaPercorsi.insets = new Insets(0, 0, 5, 5);
		gbc_btnRicercaPercorsi.gridx = 4;
		gbc_btnRicercaPercorsi.gridy = 8;
		JButton btnUtentiRegistrati = new JButton("Utenti registrati");
		btnUtentiRegistrati.setToolTipText("visualizza tutti gli utenti presenti in archivio");
		btnUtentiRegistrati.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<String > utenti= Model.elencoUtenti();
				text1.setText("Elenco utenti presenti nell'archivio: ");
				for(int i=0;i<utenti.size();i++)text1.append(utenti.get(i));
			}
		});
		GridBagConstraints gbc_btnUtentiRegistrati = new GridBagConstraints();
		gbc_btnUtentiRegistrati.insets = new Insets(0, 0, 5, 5);
		gbc_btnUtentiRegistrati.gridx = 6;
		gbc_btnUtentiRegistrati.gridy = 5;
		
		frame.getContentPane().add(btnUtentiRegistrati, gbc_btnUtentiRegistrati);
		
				JButton btnTuttiIPercorsi = new JButton("Tutti i percorsi");
				btnTuttiIPercorsi.setToolTipText("visualizza tutti i percorsi presenti in archivio");
				btnTuttiIPercorsi.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						ArrayList<String> percorsi=Model.leggiTutto();
						text1.setText("Percorsi presenti: \n");
						for(int l=0;l<percorsi.size();l++)text1.append(Model.leggiTutto().get(l));
					}
				});
						

						GridBagConstraints gbc_btnCamminoMinimo = new GridBagConstraints();
						gbc_btnCamminoMinimo.insets = new Insets(0, 0, 5, 5);
						gbc_btnCamminoMinimo.gridx = 4;
						gbc_btnCamminoMinimo.gridy = 6;
						
						
						JButton btnCamminoMinimo = new JButton("Cammino minimo");
						btnCamminoMinimo.setToolTipText("seleziona citt\u00E0 partenza, arrivo, orario partenza per ottenere il percorso pi\u00F9 breve");
						btnCamminoMinimo.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								String a = (String)partenza1.getSelectedItem();
								String b = (String)arrivo1.getSelectedItem();
								int part=((Integer) ora1.getSelectedItem()*60)+(Integer) minuti1.getSelectedItem();
								if(a.compareTo(b)==0)text1.setText("Scegli due citt� diverse!");
								else{
								ArrayList<String> cammino = Controller.camminoMinimo(a, b);
								ArrayList<Integer> orari = Controller.passaggiMinimi(a, b, part);
								text1.setText("");
								for(int l=0;l<cammino.size();l++){
									text1.append("\n"+Controller.displayTime(orari.get(l)));
									text1.append(" da arco: "+cammino.get(l));
								}
								}
							}
						});		
						
								frame.getContentPane().add(btnCamminoMinimo, gbc_btnCamminoMinimo);
				
						GridBagConstraints gbc_btnTuttiIPercorsi = new GridBagConstraints();
						gbc_btnTuttiIPercorsi.insets = new Insets(0, 0, 5, 5);
						gbc_btnTuttiIPercorsi.gridx = 6;
						gbc_btnTuttiIPercorsi.gridy = 6;
						
								frame.getContentPane().add(btnTuttiIPercorsi, gbc_btnTuttiIPercorsi);

								frame.getContentPane().add(btnRicercaPercorsi, gbc_btnRicercaPercorsi);
								
	}

}
