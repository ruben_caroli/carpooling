/**
 * Questa classe contiene tutte le funzioni
 * necessarie a scrivere e leggere il file xml
 * @author Ruben
 *
 */
package it.unibg.concarreggio.pack;

import org.jdom.*; 
import org.jdom.output.*; 
import java.util.*;
import java.awt.DisplayMode;
import java.io.*; 

import org.jdom.input.*; 


public class Model {
	
	//questa funzione riceve in input un tempo in minuti, e ritorna una stringa visualizzabile con l'orario convertito in ore e minuti
	static String displayTime(int a){
		if(a%60==0)return(""+a/60+" in punto");
		else if(a%60>=10)return(""+a/60+"."+a%60);		
		else return(""+a/60+".0"+a%60);
	}
	
/*	  public static void scrivi(String nomi, String start, String utente) { 
		    try { 
		      //Elemento radice 
		      Element root = new Element("PERCORSI"); 
		      //Documento 
		      Document document = new Document(root); 

		      //creazione della struttura del file xml
		      Element user = new Element("UTENTE"); 
		      user.setAttribute("CODICE", utente);
		      Element percorso = new Element ("PERCORSO");
		      percorso.setAttribute("numero","1");
		      user.addContent(percorso);
		      Element arco = new Element ("ARCO");
		      arco.setAttribute("numero","1");
		      percorso.addContent(arco);
		      Element ora = new Element ("START");
		      ora.setText(start);
		      arco.addContent(ora);
		      Element nomiarco = new Element("NOMI");
		      nomiarco.setText(nomi);
		      arco.addContent(nomiarco);
		      root.addContent(user);
		      
		      //Creazione dell'oggetto XMLOutputter 
		      XMLOutputter outputter = new XMLOutputter(); 
		      //Imposto il formato dell'outputter come "bel formato" 
		      outputter.setFormat(Format.getPrettyFormat()); 
		      //Produco l'output sul file xml.foo 
		      outputter.output(document, new FileOutputStream("archivio.xml")); 
		      System.out.println("File creato:"); 
		      //Stampo l'output anche sullo standard output 
		      outputter.output(document, System.out); 
		    }  
		    catch (IOException e) { 
		      System.err.println("Errore durante il parsing del documento");
		      e.printStackTrace(); 
		    } 

		}
	*/  
	/*
	  public static void appendi(String nomi, String start, String utente) { 
		  try {
		  //Creo un SAXBuilder e con esco costruisco un document 
		  SAXBuilder builder = new SAXBuilder(); 
		  Document document = builder.build(new File("archivio.xml")); 
		     
		  //Prendo la radice 
		  Element root = document.getRootElement(); 
	      Element user = new Element("UTENTE"); 
	      user.setAttribute("CODICE", utente);
	      Element percorso = new Element ("PERCORSO");
	      percorso.setAttribute("numero","1");
	      user.addContent(percorso);
	      Element arco = new Element ("ARCO");
	      arco.setAttribute("numero","1");
	      percorso.addContent(arco);
	      Element ora = new Element ("START");
	      ora.setText(start);
	      arco.addContent(ora);
	      Element nomiarco = new Element("NOMI");
	      nomiarco.setText(nomi);
	      arco.addContent(nomiarco);
	      root.addContent(user);
	      
	      //Creazione dell'oggetto XMLOutputter 
	      XMLOutputter outputter = new XMLOutputter(); 
	      //Imposto il formato dell'outputter come "bel formato" 
	      outputter.setFormat(Format.getPrettyFormat()); 
	      //Produco l'output sul file xml.foo 
	      outputter.output(document, new FileOutputStream("archivio.xml")); 
	      System.out.println("File creato:"); 
	      //Stampo l'output anche sullo standard output 
	      outputter.output(document, System.out); 
		    }  
		    catch (IOException e) { 
		      System.err.println("Errore durante il parsing del documento");
		      e.printStackTrace(); 
		    } 
		   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		  }
	  */

	  //in base al codice utente dato in ingresso, la funzione ritorna il numero dei percorsi.
	  //Pu� essere anche usata per controllare se un utente non esiste, in quel caso ritorna zero.
	  public static int contaPercorsi(String codice1){
		     int numperc=0;
		  try { 
			     //Creo un SAXBuilder e con esco costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	Element utente = (Element)iterator.next();
			    	String codice2 = utente.getAttributeValue("CODICE");
			    	if(codice1.compareTo(codice2)==0){
				        List percorsi = utente.getChildren();
				        Iterator iteratorPerc = percorsi.iterator();
				        	while(iteratorPerc.hasNext()){
				        		numperc+=1;
				        		Element percorso=(Element)iteratorPerc.next();
				        	}
			    		}
			    	}
		   }catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		  return numperc;
	  }

	  //in base al codice utente inserito, ritorna la lista dei percorsi
	  public static ArrayList<String> percorsiUtente(String cod_utente){
		  int trovato=0; 
		  ArrayList<String> ritorno=new ArrayList<String>();
		   try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	Element utente = (Element)iterator.next();
			    	System.out.println(utente.getAttributeValue("CODICE"));
			    	if(utente.getAttributeValue("CODICE").compareTo(cod_utente)==0){
			        	//se l'utente corrisponde a quello cercato, vengono elencati i percorsi
			        	System.out.println("Utente trovato.");
			        	ritorno.add("Utente trovato.");
			        	trovato=1;
			    	List percorsi = utente.getChildren();
			        Iterator iteratorPerc = percorsi.iterator();
			        j=0;
			        	while(iteratorPerc.hasNext()){
			        		j++;
			        		System.out.println("\n\tPercorso numero: "+j);
			        		ritorno.add("\n\tPercorso numero: "+j);
			        		Element percorso=(Element)iteratorPerc.next();
			        		List archi = percorso.getChildren();
			        		Iterator iteratorArchi = archi.iterator();
			        		k=0;
			        		while(iteratorArchi.hasNext()){
			        			k++;
			        			Element arco5 = (Element)iteratorArchi.next();
			        			Element avvio = arco5.getChild("START");
			        			Element nomii = arco5.getChild("NOMI");
			        			System.out.println("\n\t\tOrario: "+displayTime(Integer.parseInt(avvio.getText())));
			        			System.out.println("\n\t\tTratta: "+nomii.getText());
			        			ritorno.add("\n\t\tOrario: "+displayTime(Integer.parseInt(avvio.getText())));
			        			ritorno.add("\n\t\tTratta: "+nomii.getText());
			        		}    
			        	}
			     }
			     }
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		   if(trovato==0){
			   System.out.println("Utente non trovato.");
			   ritorno.add("Utente non trovato.");
		   }
		   return ritorno;
	  }
	  
	  //in base al codice utente e al numero percorso inserito, ritorna la lista degli archi di quel percorso
	  public static ArrayList<String> archiPercorso(String cod_utente, int numPerc){
		  int trovato=0, trovato2=0;
		  ArrayList<String> ritorno=new ArrayList<String>();
		   try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	Element utente = (Element)iterator.next();
			    	if(utente.getAttributeValue("CODICE").compareTo(cod_utente)==0){
			        	//se l'utente corrisponde a quello cercato, vengono elencati i percorsi
			        	System.out.println("Utente "+cod_utente+" trovato.");
			        	ritorno.add("Utente "+cod_utente+" trovato.");
			        	trovato=1;
			    	List percorsi = utente.getChildren();
			        Iterator iteratorPerc = percorsi.iterator();
			        j=0;
			        	while(iteratorPerc.hasNext()){
			        		j++;
			        		Element percorso=(Element)iteratorPerc.next();
			        		if(j==numPerc){
			        		ritorno.add("\nPercorso "+numPerc+" trovato.");
			        		System.out.println("Percorso "+numPerc+" trovato.");
			        		trovato2=1;
			        		List archi = percorso.getChildren();
			        		Iterator iteratorArchi = archi.iterator();
			        		k=0;
			        		while(iteratorArchi.hasNext()){
			        			k++;
			        			Element arco5 = (Element)iteratorArchi.next();
			        			Element avvio = arco5.getChild("START");
			        			Element nomii = arco5.getChild("NOMI");
			        			System.out.println("\n\t\tOrario: "+displayTime(Integer.parseInt(avvio.getText())));
			        			System.out.println("\n\t\tTratta: "+nomii.getText());
			        			ritorno.add("\n\t\tOrario: "+displayTime(Integer.parseInt(avvio.getText())));
			        			ritorno.add("\n\t\tTratta: "+nomii.getText());
			        			}    
			        		}
			        	}
			     }
			     }
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		   if(trovato==0){
			   System.out.println("Utente non trovato.");
			   ritorno.add("Utente non trovato.");
		   }else if(trovato2==0){
			   System.out.println("Percorso non trovato.");
		   }
		   return ritorno;
	  }
	  
	  public static ArrayList<String> leggiTutto(){
		  ArrayList<String> ritorno = new ArrayList<String>();
		  try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	System.out.print("\nUtente numero: "+i);
			    	Element utente = (Element)iterator.next();
			    	System.out.print("\nCodice: "+utente.getAttributeValue("CODICE"));
			    	ritorno.add("\n\nUtente numero "+i+", codice "+utente.getAttributeValue("CODICE"));
			        List percorsi = utente.getChildren();
			        Iterator iteratorPerc = percorsi.iterator();
			        j=0;
			        	while(iteratorPerc.hasNext()){
			        		j++;
			        		System.out.println("\n\tPercorso numero: "+j);
			        		ritorno.add("\n\n\tPercorso "+j);
			        		Element percorso=(Element)iteratorPerc.next();
			        		List archi = percorso.getChildren();
			        		Iterator iteratorArchi = archi.iterator();
			        		k=0;
			        		while(iteratorArchi.hasNext()){
			        			k++;
			        			System.out.println("\n\t\tArco numero: "+k);
			        			ritorno.add("\n\tArco "+k);
			        			Element arco5 = (Element)iteratorArchi.next();
			        			Element avvio = arco5.getChild("START");
			        			Element nomii = arco5.getChild("NOMI");
			        			System.out.println("\n\t\t"+avvio.getText());
			        			int tempo=Integer.parseInt(avvio.getText());
			        			ritorno.add("\n\tPassaggio alle "+displayTime(tempo));
			        			System.out.println("\n\t\t"+nomii.getText());
			        			ritorno.add(" dalle citt� "+nomii.getText());
			        		}    
			        	}
			     	} 
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		   return ritorno;
	  }

	  //questa funzione ricerca i percorsi che hanno almeno un arco in comune con quello inserito
	  static ArrayList<String> ricercaXML(String cod_utente, String citta1, String citta2, int timepart, int toll){
		     //creo la stringa che ritorner� le informazioni da visualizzare all'utente finale
		     ArrayList<String> ritorno=new ArrayList<String>();
		  try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     ArrayList<String> percmin = Controller.camminoMinimo(citta1, citta2);
			     ArrayList<Integer> tempimin = Controller.passaggiMinimi(citta1, citta2, timepart);
			 while(iterator.hasNext()){ 
			    //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    i++;
			    Element utente = (Element)iterator.next();
			    if(cod_utente.compareTo(utente.getAttributeValue("CODICE"))!=0){
			    	System.out.println("\nConfronto nuovo utente "+cod_utente+" con utente "+utente.getAttributeValue("CODICE")+":\n");
			    	List percorsi = utente.getChildren();
				    Iterator iteratorPerc = percorsi.iterator();
				    j=0;
				    while(iteratorPerc.hasNext()){
				        		j++;
				        		System.out.println("Percorso numero "+j);
				        		Element percorso=(Element)iteratorPerc.next();
				        		List archi = percorso.getChildren();
				        		Iterator iteratorArchi = archi.iterator();
				        		k=0;
				        		Double num=0.0, den=0.0;
				        		while(iteratorArchi.hasNext()){
				        			k++;
				        			Element arco5 = (Element)iteratorArchi.next();
				        			Element avvio = arco5.getChild("START");
				        			Element nomii = arco5.getChild("NOMI");
				        			String nomearco = nomii.getText();
				        			int tempoarco = Integer.parseInt(avvio.getText());
				        			den++;
				        			for(int l=0;l<percmin.size();l++){
				        				if(percmin.get(l).compareTo(nomearco)==0){
				        					if(Math.abs(tempoarco-tempimin.get(l))<=toll){
				        						num++;
				        						System.out.println("\n\tArco e orario corretti:");
				        						System.out.println("\n\tArco "+nomearco);
				        						System.out.println("\n\tOrario gi� presente: "+displayTime(tempoarco)+", orario del nuovo utente: "+displayTime(tempimin.get(l))+" (tolleranza "+toll+").\n");
				        					}
				        				}
				        			}
				        		}
				        		Double perc=(num/den)*100;
				        		System.out.println("\nPercentuale di coincidenza con questo percorso: "+perc+"\n");
				        		if(perc>0)ritorno.add("Percentuale di coincidenza con utente codice "+utente.getAttributeValue("CODICE")+" percorso numero "+j+": "+perc+"\n");
				        	}
			    		}  
			  		} 
			  }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
	  return ritorno;
	  }
	  
	  
	  //questa funzione inserisce un nuovo percorso nel file archivio, inoltre crea l'utente se non � gi� presente
	  static int insertXML(String cod_utente, String citta1, String citta2, int timepart)
	  {
		  int trovato=0;
		   try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	Element utente = (Element)iterator.next();
			    	
			    	//se l'utente � quello che stavo cercando
			        if(cod_utente.compareTo(utente.getAttributeValue("CODICE").toString())==0){
			        	System.out.print("\nCodice: "+utente.getAttributeValue("CODICE"));
			        	List percorsi = utente.getChildren();
			        	Iterator iteratorPerc = percorsi.iterator();
			        	j=0;
			        	while(iteratorPerc.hasNext()){
			        		j++;
			        		System.out.println("\n\tPercorso numero: "+j);
			        		Element percorso=(Element)iteratorPerc.next();
			        		List archi = percorso.getChildren();
			        		Iterator iteratorArchi = archi.iterator();
			        		k=0;
			        		while(iteratorArchi.hasNext()){
			        			k++;
			        			Element arco5 = (Element)iteratorArchi.next();
			        			Element avvio = arco5.getChild("START");
			        			Element nomii = arco5.getChild("NOMI");
			        			System.out.println("\n\t\t"+avvio.getText());
			        			System.out.println("\n\t\t"+nomii.getText());
			        		}    
			        	}
			        	System.out.println("Numero percorsi gi� presenti: "+j);
			        	j++;
			        	trovato=j;
			        //aggiungo all'utente il nuovo percorso
			  	      Element percorso = new Element ("PERCORSO");
				      percorso.setAttribute("numero",Integer.toString(j));
				      utente.addContent(percorso);
					  
				  	List archi = percorso.getChildren();
	        		Iterator iteratorArchi = archi.iterator();
	        		k=0;
	        		ArrayList<String> percminimo = Controller.camminoMinimo(citta1, citta2);
	        		ArrayList<Integer> tempminimo = Controller.passaggiMinimi(citta1, citta2, timepart);
	        		for(int l=0;l<percminimo.size();l++){
	        		k++;
				      Element arco = new Element ("ARCO");
				      arco.setAttribute("numero",Integer.toString(k));
				      percorso.addContent(arco);
				      Element ora = new Element ("START");
				      ora.setText(Integer.toString(tempminimo.get(l)));
				      arco.addContent(ora);
				      Element nomiarco = new Element("NOMI");
				      nomiarco.setText(percminimo.get(l)); 
				      arco.addContent(nomiarco);
	        		}
	        		
				      //Creazione dell'oggetto XMLOutputter 
				      XMLOutputter outputter = new XMLOutputter(); 
				      //Imposto il formato dell'outputter come "bel formato" 
				      outputter.setFormat(Format.getPrettyFormat()); 
				      //Produco l'output sul file xml.foo 
				      outputter.output(document, new FileOutputStream("archivio.xml")); 
			        }
			     }
			     //se l'utente non � gi� presente, viene creato
			     if(trovato==0){
			    	ArrayList<String> percminimo = Controller.camminoMinimo(citta1, citta2);
		        	ArrayList<Integer> tempminimo = Controller.passaggiMinimi(citta1, citta2, timepart);
			    	System.out.println("\nUtente creato nell'archivio.");
			    	//crea il nuovo utente e aggiunge il primo percorso
				    Element user = new Element("UTENTE"); 
				    user.setAttribute("CODICE", cod_utente);
				    root.addContent(user);
				      Element percorso = new Element ("PERCORSO");
				      percorso.setAttribute("numero","1");
				      user.addContent(percorso);
				      //tramite un ciclo si creano i vari archi e vengono aggiunti al percorso numero 1
				      for(int l=0;l<percminimo.size();l++){
			        		k++;
						      Element arco = new Element ("ARCO");
						      arco.setAttribute("numero",Integer.toString(k));
						      percorso.addContent(arco);
						      Element ora = new Element("START");
						      ora.setText(Integer.toString(tempminimo.get(l)));
						      arco.addContent(ora);
						      Element nomiarco = new Element ("NOMI");
						      nomiarco.setText(percminimo.get(l));
						      arco.addContent(nomiarco);
			        		}
				      
				      //Creazione dell'oggetto XMLOutputter 
				      XMLOutputter outputter = new XMLOutputter(); 
				      //Imposto il formato dell'outputter come "bel formato" 
				      outputter.setFormat(Format.getPrettyFormat()); 
				      //Produco l'output sul file xml.foo 
				      outputter.output(document, new FileOutputStream("archivio.xml"));
			     }
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file");
			     e.printStackTrace(); 
			   }
		   return trovato;
	  }
	  
	  //questa funzione ritorna l'elenco di tutti gli utenti presenti gi� nell'archivio XML
	  public static ArrayList<String> elencoUtenti(){
		  ArrayList<String> ritorno = new ArrayList<String>();
		  try { 
			     //Creo un SAXBuilder e costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio
			     int i=0,j=0,k=0;
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "UTENTE" e dei suoi figli percorsi iterativamente
			    	i++;
			    	System.out.print("\nUtente numero: "+i);
			    	ritorno.add("\nUtente numero: "+i);
			    	Element utente = (Element)iterator.next();
			    	System.out.print("\nCodice: "+utente.getAttributeValue("CODICE"));
			    	ritorno.add("\nCodice: "+utente.getAttributeValue("CODICE"));
			    	System.out.println("\nPercorsi: "+contaPercorsi(utente.getAttributeValue("CODICE")));
			    	ritorno.add("\nPercorsi: "+contaPercorsi(utente.getAttributeValue("CODICE")));
			     } 
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		   return ritorno;
	  }
	  
	  
	  public static void main(String[] args){
	  //appendi("brescia : verona","1660","49907");
	  //leggi();
	//int u=contaPercorsi("49907");
		//  System.out.println("Numero percorsi:"+u);
		  //insertXML("005", "prato", "milano", 1000);
		 //ricercaXML("012","bologna","bergamo",1058,10);
		 //percorsiUtente("003");
		  //archiPercorso("003", 10);
		  //elencoUtenti();
		  leggiTutto();
	  }
}