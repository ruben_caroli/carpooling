package it.unibg.concarreggio.pack;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.jgrapht.GraphPath;
import org.jgrapht.alg.DijkstraShortestPath;
import org.jgrapht.graph.DefaultWeightedEdge;
import org.jgrapht.graph.SimpleDirectedWeightedGraph;


public class Controller {

	//questa funzione si occupa di creare il grafo con le citt� e i pesi degli archi, viene usata dalle altre funzioni della classe
	public static SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> creaMappa(){
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> w = new SimpleDirectedWeightedGraph<String, DefaultWeightedEdge>(DefaultWeightedEdge.class);

		//si inizializzano le stringhe che serviranno a creare i vertici del grafo
		String mi = "milano";
	    String to = "torino";
	    String bg = "bergamo";
	    String ve = "venezia";
	    String bo = "bologna";
	    String bs = "brescia";
	    String vr = "verona";
	    String pd = "padova";
	    String bz = "bolzano";
	    String ge = "genova";
	    String pr = "prato";
	    String pg = "perugia";
	    
	    //si creano i vertici tramite le stringhe
	    w.addVertex(mi);
	    w.addVertex(to);
	    w.addVertex(bo);
	    w.addVertex(ve);
	    w.addVertex(bg);
	    w.addVertex(bs);
	    w.addVertex(vr);
	    w.addVertex(pd);
	    w.addVertex(bz);
	    w.addVertex(ge);
	    w.addVertex(pr);
	    w.addVertex(pg);
	    
	    //si creano gli archi pesati nelle due direzioni
	    //archi da una direzione
	    w.setEdgeWeight(w.addEdge(to, mi),100);
	    w.setEdgeWeight(w.addEdge(mi, bg),50);
	    w.setEdgeWeight(w.addEdge(bg, bs),50);
	    w.setEdgeWeight(w.addEdge(bs, vr),40);
	    w.setEdgeWeight(w.addEdge(vr, pd),40);
	    w.setEdgeWeight(w.addEdge(pd, ve),20);
	    w.setEdgeWeight(w.addEdge(vr, bz),30);
	    w.setEdgeWeight(w.addEdge(bs, bo),66);
	    w.setEdgeWeight(w.addEdge(vr, bo),40);
	    w.setEdgeWeight(w.addEdge(pd, bo),75);
	    w.setEdgeWeight(w.addEdge(ge, bo),160);
	    w.setEdgeWeight(w.addEdge(mi, ge),125);
	    w.setEdgeWeight(w.addEdge(ge, pr),120);
	    w.setEdgeWeight(w.addEdge(bo, pr),57);
	    w.setEdgeWeight(w.addEdge(bo, pg),215);
	    w.setEdgeWeight(w.addEdge(pr, pg),237);
	    
	    //archi nell'altra direzione
	    w.setEdgeWeight(w.addEdge(mi, to),100);
	    w.setEdgeWeight(w.addEdge(bg, mi),50);
	    w.setEdgeWeight(w.addEdge(bs, bg),50);
	    w.setEdgeWeight(w.addEdge(vr, bs),40);
	    w.setEdgeWeight(w.addEdge(pd, vr),40);
	    w.setEdgeWeight(w.addEdge(ve, pd),20);
	    w.setEdgeWeight(w.addEdge(bz, vr),30);
	    w.setEdgeWeight(w.addEdge(bo, bs),66);
	    w.setEdgeWeight(w.addEdge(bo, vr),40);
	    w.setEdgeWeight(w.addEdge(bo, pd),75);
	    w.setEdgeWeight(w.addEdge(bo, ge),160);
	    w.setEdgeWeight(w.addEdge(ge, mi),125);
	    w.setEdgeWeight(w.addEdge(pr, ge),120);
	    w.setEdgeWeight(w.addEdge(pr, bo),57);
	    w.setEdgeWeight(w.addEdge(pg, bo),215);
	    w.setEdgeWeight(w.addEdge(pg, pr),237);
		
		return w;
	}
	
	//camminoMinimo prende in input due citt� dalla gui, e trova il cammino minimo sul grafo creato da creaMappa
	public static ArrayList<String> camminoMinimo(String a, String b){
		ArrayList<String> listaString = new ArrayList<String>();
		if(a==b){
			listaString.add("0");
			return listaString;
		}
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> w = creaMappa();
		DijkstraShortestPath<String, DefaultWeightedEdge> strada = new DijkstraShortestPath<String, DefaultWeightedEdge>(w, a, b);
	    GraphPath<String, DefaultWeightedEdge> cammino = strada.getPath();
		java.util.List <DefaultWeightedEdge> lista = cammino.getEdgeList();
		for(int i=0; i<lista.size();i++){
			listaString.add((lista.get(i)).toString());
		}
		return listaString;
	}
	
	/**passaggiMinimi ritorna una lista di stringhe che costituiscono gli orari di passaggio
	dell'utente in ogni citt�, senza le tolleranze, in base alle citt� di partenza-arrivo e all'orario di partenza*/
	public static ArrayList<Integer> passaggiMinimi(String a, String b, int tempoPart){
		ArrayList<Integer> listaTempi = new ArrayList<Integer>();
		if(a==b){
			listaTempi.add(-1);
			return listaTempi;
		}
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> w = creaMappa();
		DijkstraShortestPath<String, DefaultWeightedEdge> strada = new DijkstraShortestPath<String, DefaultWeightedEdge>(w, a, b);
	    GraphPath<String, DefaultWeightedEdge> cammino = strada.getPath();
	    List<DefaultWeightedEdge> listaArchi = cammino.getEdgeList();
		int ora_passaggio=tempoPart;
		
		for(int i=0;i<listaArchi.size();i++)
		{
			listaTempi.add(ora_passaggio);
			ora_passaggio=(ora_passaggio+(int)(w.getEdgeWeight(listaArchi.get(i))));
		}
		return listaTempi;
	}
	
	//questa funzione riceve in input un tempo in minuti, e ritorna una stringa visualizzabile con l'orario convertito in ore e minuti
	static String displayTime(int a){
		if(a%60==0)return("Ora passaggio: "+a/60+" in punto");
		else if(a%60>=10)return("Ora passaggio: "+a/60+"."+a%60);		
		else return("Ora passaggio: "+a/60+".0"+a%60);
	}
	
	//funzione percorso comune, riceve in input 4 citt� e definisce quali archi in comune ci sono fra i percorsi a->b e c->d, con partenze e ed f in minuti
	public static ArrayList<String> perCom(String a, String b, String c, String d, int e, int f, int toll1, int toll2){
		java.util.List<String> lista1 = camminoMinimo(a, b);
		java.util.List<String> lista2 = camminoMinimo(c, d);
		ArrayList<String> comune = new ArrayList<String>();
		ArrayList<Integer> lista3 = passaggiMinimi(a, b, e);
		ArrayList<Integer> lista4 = passaggiMinimi(c, d, f);
		
		if(lista1.get(0)=="0"||lista2.get(0)=="0"){
			comune.add("-1");
			return comune;
			}
		
		String x,y;
		int w,z;
		for(int i=0;i<lista1.size();i++)
			for(int j=0;j<lista2.size();j++)
			{
			x = lista1.get(i);
			y = lista2.get(j);
			w = lista3.get(i);
			z = lista4.get(j);
			
			//se coincide l'arco e anche il tempo aggiunge l'arco nella lista
			if(x.compareTo(y)==0)if(Math.abs(w-z)<=(toll1+toll2))comune.add(x);
			}
		return comune;
	}
	
	//questa funzione crea una lista delle citt� contenute nel grafo creato da creaMappa
	public static Set<String> listaPercorso(){
		SimpleDirectedWeightedGraph<String, DefaultWeightedEdge> w = creaMappa();
		Set<String> e = w.vertexSet();
		return e;
	}
	
	public static void main(String[] args) {
		
	}
	
}
