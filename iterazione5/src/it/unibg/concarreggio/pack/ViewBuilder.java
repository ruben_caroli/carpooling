package it.unibg.concarreggio.pack;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.JScrollPane;
import javax.swing.JTextField;


public class ViewBuilder {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewBuilder window = new ViewBuilder();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewBuilder() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1044, 628);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 1.0, 1.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 1.0, 1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JLabel lblUtente = new JLabel("Utente 1");
		lblUtente.setForeground(Color.BLUE);
		lblUtente.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblUtente = new GridBagConstraints();
		gbc_lblUtente.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtente.gridx = 1;
		gbc_lblUtente.gridy = 1;
		frame.getContentPane().add(lblUtente, gbc_lblUtente);
		
		JLabel lblUtente_1 = new JLabel("Utente 2");
		lblUtente_1.setForeground(Color.BLUE);
		lblUtente_1.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblUtente_1 = new GridBagConstraints();
		gbc_lblUtente_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtente_1.gridx = 9;
		gbc_lblUtente_1.gridy = 1;
		frame.getContentPane().add(lblUtente_1, gbc_lblUtente_1);
		
		final JComboBox partenza1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_partenza1 = new GridBagConstraints();
		gbc_partenza1.insets = new Insets(0, 0, 5, 5);
		gbc_partenza1.fill = GridBagConstraints.HORIZONTAL;
		gbc_partenza1.gridx = 1;
		gbc_partenza1.gridy = 2;
		frame.getContentPane().add(partenza1, gbc_partenza1);
		
		final JComboBox partenza2 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_partenza2 = new GridBagConstraints();
		gbc_partenza2.insets = new Insets(0, 0, 5, 5);
		gbc_partenza2.fill = GridBagConstraints.HORIZONTAL;
		gbc_partenza2.gridx = 9;
		gbc_partenza2.gridy = 2;
		frame.getContentPane().add(partenza2, gbc_partenza2);
		
		final JComboBox arrivo1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_arrivo1 = new GridBagConstraints();
		gbc_arrivo1.insets = new Insets(0, 0, 5, 5);
		gbc_arrivo1.fill = GridBagConstraints.HORIZONTAL;
		gbc_arrivo1.gridx = 1;
		gbc_arrivo1.gridy = 3;
		frame.getContentPane().add(arrivo1, gbc_arrivo1);
		
		final JComboBox arrivo2 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_arrivo2 = new GridBagConstraints();
		gbc_arrivo2.insets = new Insets(0, 0, 5, 5);
		gbc_arrivo2.fill = GridBagConstraints.HORIZONTAL;
		gbc_arrivo2.gridx = 9;
		gbc_arrivo2.gridy = 3;
		frame.getContentPane().add(arrivo2, gbc_arrivo2);
		
		JLabel lblOrarioPartenza = new JLabel("Orario partenza:");
		GridBagConstraints gbc_lblOrarioPartenza = new GridBagConstraints();
		gbc_lblOrarioPartenza.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrarioPartenza.anchor = GridBagConstraints.EAST;
		gbc_lblOrarioPartenza.gridx = 0;
		gbc_lblOrarioPartenza.gridy = 4;
		frame.getContentPane().add(lblOrarioPartenza, gbc_lblOrarioPartenza);
		
		ArrayList<Integer> orari = new ArrayList<Integer>();
		for(int i=0;i<24;i++)orari.add(i);

		ArrayList<Integer> minuti = new ArrayList<Integer>();
		for(int i=0;i<60;i+=5)minuti.add(i);
		
		ArrayList<Integer> tolleranze = new ArrayList<Integer>();
		for(int i=0;i<30;i+=1)tolleranze.add(i);
		
		final JComboBox ora1 = new JComboBox(orari.toArray());
		GridBagConstraints gbc_ora1 = new GridBagConstraints();
		gbc_ora1.insets = new Insets(0, 0, 5, 5);
		gbc_ora1.fill = GridBagConstraints.HORIZONTAL;
		gbc_ora1.gridx = 1;
		gbc_ora1.gridy = 4;
		frame.getContentPane().add(ora1, gbc_ora1);
		
		final JComboBox minuti1 = new JComboBox(minuti.toArray());
		GridBagConstraints gbc_minuti1 = new GridBagConstraints();
		gbc_minuti1.insets = new Insets(0, 0, 5, 5);
		gbc_minuti1.fill = GridBagConstraints.HORIZONTAL;
		gbc_minuti1.gridx = 2;
		gbc_minuti1.gridy = 4;
		frame.getContentPane().add(minuti1, gbc_minuti1);
		
		JLabel lblOrarioPartenza_1 = new JLabel("Orario partenza:");
		GridBagConstraints gbc_lblOrarioPartenza_1 = new GridBagConstraints();
		gbc_lblOrarioPartenza_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblOrarioPartenza_1.anchor = GridBagConstraints.EAST;
		gbc_lblOrarioPartenza_1.gridx = 8;
		gbc_lblOrarioPartenza_1.gridy = 4;
		frame.getContentPane().add(lblOrarioPartenza_1, gbc_lblOrarioPartenza_1);
		
		final JComboBox ora2 = new JComboBox(orari.toArray());
		GridBagConstraints gbc_ora2 = new GridBagConstraints();
		gbc_ora2.insets = new Insets(0, 0, 5, 5);
		gbc_ora2.fill = GridBagConstraints.HORIZONTAL;
		gbc_ora2.gridx = 9;
		gbc_ora2.gridy = 4;
		frame.getContentPane().add(ora2, gbc_ora2);
		
		final JComboBox minuti2 = new JComboBox(minuti.toArray());
		GridBagConstraints gbc_minuti2 = new GridBagConstraints();
		gbc_minuti2.insets = new Insets(0, 0, 5, 5);
		gbc_minuti2.fill = GridBagConstraints.HORIZONTAL;
		gbc_minuti2.gridx = 10;
		gbc_minuti2.gridy = 4;
		frame.getContentPane().add(minuti2, gbc_minuti2);
		
		JLabel lblTolleranza = new JLabel("Tolleranza:");
		GridBagConstraints gbc_lblTolleranza = new GridBagConstraints();
		gbc_lblTolleranza.insets = new Insets(0, 0, 5, 5);
		gbc_lblTolleranza.anchor = GridBagConstraints.EAST;
		gbc_lblTolleranza.gridx = 0;
		gbc_lblTolleranza.gridy = 5;
		frame.getContentPane().add(lblTolleranza, gbc_lblTolleranza);
		
		final JComboBox toll1 = new JComboBox(tolleranze.toArray());
		GridBagConstraints gbc_toll1 = new GridBagConstraints();
		gbc_toll1.insets = new Insets(0, 0, 5, 5);
		gbc_toll1.fill = GridBagConstraints.HORIZONTAL;
		gbc_toll1.gridx = 1;
		gbc_toll1.gridy = 5;
		frame.getContentPane().add(toll1, gbc_toll1);
		
		JLabel lblMin = new JLabel("min");
		GridBagConstraints gbc_lblMin = new GridBagConstraints();
		gbc_lblMin.insets = new Insets(0, 0, 5, 5);
		gbc_lblMin.gridx = 2;
		gbc_lblMin.gridy = 5;
		frame.getContentPane().add(lblMin, gbc_lblMin);
		
		JLabel lblTolleranza_1 = new JLabel("Tolleranza");
		GridBagConstraints gbc_lblTolleranza_1 = new GridBagConstraints();
		gbc_lblTolleranza_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblTolleranza_1.anchor = GridBagConstraints.EAST;
		gbc_lblTolleranza_1.gridx = 8;
		gbc_lblTolleranza_1.gridy = 5;
		frame.getContentPane().add(lblTolleranza_1, gbc_lblTolleranza_1);
		
		final JComboBox toll2 = new JComboBox(tolleranze.toArray());
		GridBagConstraints gbc_toll2 = new GridBagConstraints();
		gbc_toll2.insets = new Insets(0, 0, 5, 5);
		gbc_toll2.fill = GridBagConstraints.HORIZONTAL;
		gbc_toll2.gridx = 9;
		gbc_toll2.gridy = 5;
		frame.getContentPane().add(toll2, gbc_toll2);
		
		JLabel lblMin_1 = new JLabel("min");
		GridBagConstraints gbc_lblMin_1 = new GridBagConstraints();
		gbc_lblMin_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblMin_1.gridx = 10;
		gbc_lblMin_1.gridy = 5;
		frame.getContentPane().add(lblMin_1, gbc_lblMin_1);
		
		final JTextArea text1 = new JTextArea();
		text1.setWrapStyleWord(true);
		text1.setLineWrap(true);
		GridBagConstraints gbc_text1 = new GridBagConstraints();
		gbc_text1.gridwidth = 4;
		gbc_text1.insets = new Insets(0, 0, 0, 5);
		gbc_text1.fill = GridBagConstraints.BOTH;
		gbc_text1.gridx = 0;
		gbc_text1.gridy = 9;
		frame.getContentPane().add(text1, gbc_text1);
		
		final JTextArea text3 = new JTextArea();
		text3.setWrapStyleWord(true);
		text3.setLineWrap(true);
		GridBagConstraints gbc_text3 = new GridBagConstraints();
		gbc_text3.gridwidth = 2;
		gbc_text3.insets = new Insets(0, 0, 0, 5);
		gbc_text3.fill = GridBagConstraints.BOTH;
		gbc_text3.gridx = 5;
		gbc_text3.gridy = 9;
		frame.getContentPane().add(text3, gbc_text3);
		
		final JTextArea text2 = new JTextArea();
		text2.setWrapStyleWord(true);
		text2.setLineWrap(true);
		GridBagConstraints gbc_text2 = new GridBagConstraints();
		gbc_text2.gridwidth = 4;
		gbc_text2.insets = new Insets(0, 0, 0, 5);
		gbc_text2.fill = GridBagConstraints.BOTH;
		gbc_text2.gridx = 8;
		gbc_text2.gridy = 9;
		frame.getContentPane().add(text2, gbc_text2);
		
		
		JButton btnCalcola = new JButton("Percorso comune");
		
				GridBagConstraints gbc_btnCalcola = new GridBagConstraints();
				gbc_btnCalcola.gridwidth = 2;
				gbc_btnCalcola.insets = new Insets(0, 0, 5, 5);
				gbc_btnCalcola.gridx = 5;
				gbc_btnCalcola.gridy = 7;
				frame.getContentPane().add(btnCalcola, gbc_btnCalcola);
				
				btnCalcola.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String a = (String)partenza1.getSelectedItem();
						String b = (String)arrivo1.getSelectedItem();
						String c = (String)partenza2.getSelectedItem();
						String d = (String)arrivo2.getSelectedItem();
						
						//le ore e i minuti vengono convertiti in minuti per darli in pasto alla funzione passaggiMinimi
						int e=((Integer) ora1.getSelectedItem()*60)+(Integer) minuti1.getSelectedItem();
						int f=((Integer) ora2.getSelectedItem()*60)+(Integer) minuti2.getSelectedItem();
						
						int tolleranza1=(Integer)toll1.getSelectedItem();
						int tolleranza2=(Integer)toll2.getSelectedItem();
						
						ArrayList<String> cammino1 = Controller.camminoMinimo(a, b);
						ArrayList<Integer> tempi = (Controller.passaggiMinimi(a,b,e));
						
						text1.setText("");
						for(int i=0;i<tempi.size();i++)
						{
							text1.append("\n"+cammino1.get(i));
							text1.append("\n"+Controller.displayTime(tempi.get(i))+"\n");
						}
						
						text2.setText("");
						ArrayList<String> cammino2 = Controller.camminoMinimo(c, d);
						ArrayList<Integer> tempi2 = (Controller.passaggiMinimi(c,d,f));
						for(int i=0;i<tempi2.size();i++)
						{
							text2.append("\n"+cammino2.get(i));
							text2.append("\n"+Controller.displayTime(tempi2.get(i))+"\n");
						}
						
						//gli archi in comune vengono visualizzati tramite questa funzione
						text3.setText("");
						ArrayList<String> comune = Controller.perCom(a, b, c, d, e, f, tolleranza1, tolleranza2);
						for(int i=0;i<comune.size();i++){
							text3.append(comune.get(i)+"\n");
						}
					}
				});


		JLabel lblPercorso = new JLabel("Percorso 1:");
		GridBagConstraints gbc_lblPercorso = new GridBagConstraints();
		gbc_lblPercorso.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercorso.gridx = 1;
		gbc_lblPercorso.gridy = 8;
		frame.getContentPane().add(lblPercorso, gbc_lblPercorso);
		
		JLabel lblParteComune = new JLabel("Parte comune:");
		GridBagConstraints gbc_lblParteComune = new GridBagConstraints();
		gbc_lblParteComune.insets = new Insets(0, 0, 5, 5);
		gbc_lblParteComune.gridx = 5;
		gbc_lblParteComune.gridy = 8;
		frame.getContentPane().add(lblParteComune, gbc_lblParteComune);
		
		JLabel lblPercorso_1 = new JLabel("Percorso 2:");
		GridBagConstraints gbc_lblPercorso_1 = new GridBagConstraints();
		gbc_lblPercorso_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercorso_1.gridx = 9;
		gbc_lblPercorso_1.gridy = 8;
		frame.getContentPane().add(lblPercorso_1, gbc_lblPercorso_1);
		


	}

}
