package it.unibg.concarreggio.pack;

import org.jdom.*; 
import org.jdom.output.*; 
import java.io.*; 

public class JDOMWriter { 

  public static void main(String[] args) { 
    try { 
      //Elemento radice 
      Element root = new Element("PERCORSI"); 
      //Documento 
      Document document = new Document(root); 

      //Creazione di tre elementi figli denominato ITEM 
      //a ciascuno dei quali vengono settati tre attributi 
      //e viene aggiunto un elemento figlio contenente 
      //la descrizione della cosa da fare

      Element item2 = new Element("ITEM"); 
      item2.setAttribute("importanza", "7"); 
      item2.setAttribute("perc_completamento", "100"); 
      item2.setAttribute("completata", "si"); 
      Element descr2 = new Element("DESCR"); 
      descr2.setText("Stendere i panni"); 
      item2.addContent(descr2); 
      root.addContent(item2); 

      Element item3 = new Element("ITEM"); 
      item3.setAttribute("importanza", "4"); 
      item3.setAttribute("perc_completamento", "0"); 
      item3.setAttribute("completata", "no"); 
      Element descr3 = new Element("DESCR"); 
      descr3.setText("Cucinare"); 
      item3.addContent(descr3); 
      root.addContent(item3); 

      Element item4 = new Element("ITEM"); 
      item4.setAttribute("importanza", "4"); 
      item4.setAttribute("perc_completamento", "56"); 
      item4.setAttribute("completata", "no"); 
      Element descr4 = new Element("DESCR"); 
      descr4.setText("Programmare"); 
      item4.addContent(descr4); 
      root.addContent(item4);
      
      //Creazione dell'oggetto XMLOutputter 
      XMLOutputter outputter = new XMLOutputter(); 
      //Imposto il formato dell'outputter come "bel formato" 
      outputter.setFormat(Format.getPrettyFormat()); 
      //Produco l'output sul file xml.foo 
      outputter.output(document, new FileOutputStream("foo.xml")); 
      System.out.println("File creato:"); 
      //Stampo l'output anche sullo standard output 
      outputter.output(document, System.out); 
    }  
    catch (IOException e) { 
      System.err.println("Errore durante il parsing del documento");
      e.printStackTrace(); 
    } 

}}