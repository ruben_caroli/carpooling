/**
 * Questa classe contiene tutte le funzioni
 * necessarie a scrivere e leggere il file xml
 * @author Ruben
 *
 */
package it.unibg.concarreggio.pack;

import org.jdom.*; 
import org.jdom.output.*; 
import java.util.*;
import java.io.*; 

import org.jdom.input.*; 


public class Model {
	  public static void scrivi(String nomi, String start, String utente) { 
		    try { 
		      //Elemento radice 
		      Element root = new Element("PERCORSI"); 
		      //Documento 
		      Document document = new Document(root); 

		      //creazione della struttura del file xml
		      Element user = new Element("UTENTE"); 
		      user.setAttribute("CODICE", utente);
		      Element percorso = new Element ("PERCORSO");
		      percorso.setAttribute("numero","1");
		      user.addContent(percorso);
		      Element arco = new Element ("ARCO");
		      arco.setAttribute("numero","1");
		      percorso.addContent(arco);
		      Element ora = new Element ("START");
		      ora.setText(start);
		      arco.addContent(ora);
		      Element nomiarco = new Element("NOMI");
		      nomiarco.setText(nomi);
		      arco.addContent(nomiarco);
		      root.addContent(user);
		      
		      //Creazione dell'oggetto XMLOutputter 
		      XMLOutputter outputter = new XMLOutputter(); 
		      //Imposto il formato dell'outputter come "bel formato" 
		      outputter.setFormat(Format.getPrettyFormat()); 
		      //Produco l'output sul file xml.foo 
		      outputter.output(document, new FileOutputStream("archivio.xml")); 
		      System.out.println("File creato:"); 
		      //Stampo l'output anche sullo standard output 
		      outputter.output(document, System.out); 
		    }  
		    catch (IOException e) { 
		      System.err.println("Errore durante il parsing del documento");
		      e.printStackTrace(); 
		    } 

		}
	  
	  public static void appendi(String nomi, String start, String utente) { 
		  try {
		  //Creo un SAXBuilder e con esco costruisco un document 
		  SAXBuilder builder = new SAXBuilder(); 
		  Document document = builder.build(new File("archivio.xml")); 
		     
		  //Prendo la radice 
		  Element root = document.getRootElement(); 
	      Element user = new Element("UTENTE"); 
	      user.setAttribute("CODICE", utente);
	      Element percorso = new Element ("PERCORSO");
	      percorso.setAttribute("numero","1");
	      user.addContent(percorso);
	      Element arco = new Element ("ARCO");
	      arco.setAttribute("numero","1");
	      percorso.addContent(arco);
	      Element ora = new Element ("START");
	      ora.setText(start);
	      arco.addContent(ora);
	      Element nomiarco = new Element("NOMI");
	      nomiarco.setText(nomi);
	      arco.addContent(nomiarco);
	      root.addContent(user);
	      
	      //Creazione dell'oggetto XMLOutputter 
	      XMLOutputter outputter = new XMLOutputter(); 
	      //Imposto il formato dell'outputter come "bel formato" 
	      outputter.setFormat(Format.getPrettyFormat()); 
	      //Produco l'output sul file xml.foo 
	      outputter.output(document, new FileOutputStream("archivio.xml")); 
	      System.out.println("File creato:"); 
	      //Stampo l'output anche sullo standard output 
	      outputter.output(document, System.out); 
		    }  
		    catch (IOException e) { 
		      System.err.println("Errore durante il parsing del documento");
		      e.printStackTrace(); 
		    } 
		   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
		  }
	  
	  public static void leggi(){
		   try { 
			     //Creo un SAXBuilder e con esco costruisco un document 
			     SAXBuilder builder = new SAXBuilder(); 
			     Document document = builder.build(new File("archivio.xml")); 
			     
			      //Prendo la radice 
			     Element root = document.getRootElement(); 
			     //Estraggo i figli dalla radice 
			     List children = root.getChildren(); 
			     Iterator iterator = children.iterator(); 
			     
			      //Per ogni figlio 
			     while(iterator.hasNext()){ 
			        //Mostro il valore dell'elemento figlio "DESCR" e degli 
			        //attributi "importanza", "perc_completamento", e "completata" 
			        //sullo standard output 
			        Element arco = (Element)iterator.next();
			        Element part = arco.getChild("PARTENZA");
			        Element arr = arco.getChild("ARRIVO");
			        System.out.println("\n"+part.getText());
			        System.out.println("\n"+arr.getText());
			        
			        
			        /*System.out.println("*" + description.getText()); 
			        System.out.println("\tImportanza: " + item.getAttributeValue("importanza")); 
			        System.out.println("\tCompletamento: " + item.getAttributeValue("perc_completamento") + "%");
			        System.out.println("\tItem copmletata: " + item.getAttributeValue("completata")+"\n"); 
			     */
			     } 
			   }  
			   catch (Exception e) { 
			     System.err.println("Errore durante la lettura dal file"); 
			     e.printStackTrace(); 
			   }
	  }
	  
	  public static void main(String[] args){
	  appendi("brescia : verona","1660","49907");
	  //leggi();
	  }
}