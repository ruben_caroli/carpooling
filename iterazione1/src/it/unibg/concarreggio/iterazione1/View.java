package it.unibg.concarreggio.iterazione1;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


/**
 * In questa classe viene creata l'interfaccia grafica principale del programma, e si utilizzano le funzioni della classe Controller per calcolare gli output
 * @author Ruben
 *
 */

public class View implements ActionListener{

	//elementi interfaccia grafica
	JFrame frame = new JFrame("Car Pooling");
	JPanel panel = new JPanel(new FlowLayout());
	JButton calcola = new JButton("calcola");
	JLabel etichetta1 = new JLabel("Citt� di partenza: ");
	JLabel etichetta2 = new JLabel("Citt� di arrivo: ");
	
	JComboBox partenza = new JComboBox(Controller.listaPercorso().toArray());
	JComboBox arrivo = new JComboBox(Controller.listaPercorso().toArray());
	JTextArea text = new JTextArea(10,20);
	
	public void gui(){	
		frame.setVisible(true);
		frame.setSize(320, 270);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel);

		calcola.addActionListener(this);
		
		panel.add(etichetta1);
		panel.add(partenza);
		panel.add(etichetta2);
		panel.add(arrivo);
		panel.add(calcola);
		panel.add(text);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if(source==calcola){
			String a = (String)partenza.getSelectedItem();
			String b = (String)arrivo.getSelectedItem();
			text.setText((Controller.camminoMinimo(a, b)).toString());
		}
	}
	
	public static void main(String[] args) {
		View exe = new View();
		exe.gui();
	}
}
