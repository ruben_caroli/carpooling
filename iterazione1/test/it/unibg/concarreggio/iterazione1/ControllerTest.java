package it.unibg.concarreggio.iterazione1;

import java.util.ArrayList;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ControllerTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testCamminoMinimo() {
		String a="milano";
		String b="bergamo";
		ArrayList<String> c = Controller.camminoMinimo(a, b);
		ArrayList<String> expected = new ArrayList<String>();
		expected.add("(milano : bergamo)");
		Assert.assertNotNull(c);
		Assert.assertEquals(expected, c);
		
		System.out.println(c);
	}

	public static void main(String[] args) {
		ControllerTest a = new ControllerTest();
		a.testCamminoMinimo();
	}
}