
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;


/**
 * In questa classe viene creata l'interfaccia grafica principale del programma,
 * e si utilizzano le funzioni della classe Controller per calcolare gli output.
 * @author Ruben
 *
 */

public class View implements ActionListener{

	//elementi interfaccia grafica
	JFrame frame = new JFrame("Car Pooling");
	JPanel panel1 = new JPanel(new FlowLayout());
	
	JButton calcola1 = new JButton("Percorso comune");
	JComboBox partenza1 = new JComboBox(Controller.listaPercorso().toArray());
	JComboBox arrivo1 = new JComboBox(Controller.listaPercorso().toArray());
	JTextArea text1 = new JTextArea(10,10);

	JComboBox partenza2 = new JComboBox(Controller.listaPercorso().toArray());
	JComboBox arrivo2 = new JComboBox(Controller.listaPercorso().toArray());
	JTextArea text2 = new JTextArea(10,10);

	JTextArea text3 = new JTextArea(10,10);

	
	public void gui(){	
		frame.setVisible(true);
		frame.setSize(640, 480);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.add(panel1);

		calcola1.addActionListener(this);
		
		panel1.add(partenza1);
		panel1.add(arrivo1);
		panel1.add(text1);

		panel1.add(partenza2);
		panel1.add(arrivo2);
		panel1.add(text2);
		panel1.add(text3);
		
		panel1.add(calcola1);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		Object source = e.getSource();
		if(source==calcola1){
			String a = (String)partenza1.getSelectedItem();
			String b = (String)arrivo1.getSelectedItem();
			String c = (String)partenza2.getSelectedItem();
			String d = (String)arrivo2.getSelectedItem();
			text1.setText((Controller.camminoMinimo(a, b)).toString());
			text2.setText((Controller.camminoMinimo(c, d)).toString());
			text3.setText(Controller.perCom(a, b, c, d).toString());
		}		
	}
	
	public static void main(String[] args) {
		View exe = new View();
		exe.gui();
	}
}
