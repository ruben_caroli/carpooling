
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JComboBox;
import java.awt.Color;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JScrollPane;


public class ViewBuilder {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ViewBuilder window = new ViewBuilder();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewBuilder() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0};
		gridBagLayout.columnWeights = new double[]{0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		frame.getContentPane().setLayout(gridBagLayout);
		
		JButton btnCalcola = new JButton("Percorso comune");
		
		JLabel lblUtente = new JLabel("Utente 1");
		lblUtente.setForeground(Color.BLUE);
		lblUtente.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblUtente = new GridBagConstraints();
		gbc_lblUtente.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtente.gridx = 1;
		gbc_lblUtente.gridy = 1;
		frame.getContentPane().add(lblUtente, gbc_lblUtente);
		
		JLabel lblUtente_1 = new JLabel("Utente 2");
		lblUtente_1.setForeground(Color.BLUE);
		lblUtente_1.setBackground(Color.BLUE);
		GridBagConstraints gbc_lblUtente_1 = new GridBagConstraints();
		gbc_lblUtente_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblUtente_1.gridx = 9;
		gbc_lblUtente_1.gridy = 1;
		frame.getContentPane().add(lblUtente_1, gbc_lblUtente_1);
		
		final JComboBox partenza1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_partenza1 = new GridBagConstraints();
		gbc_partenza1.insets = new Insets(0, 0, 5, 5);
		gbc_partenza1.fill = GridBagConstraints.HORIZONTAL;
		gbc_partenza1.gridx = 1;
		gbc_partenza1.gridy = 2;
		frame.getContentPane().add(partenza1, gbc_partenza1);
		
		final JComboBox partenza2 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_partenza2 = new GridBagConstraints();
		gbc_partenza2.insets = new Insets(0, 0, 5, 5);
		gbc_partenza2.fill = GridBagConstraints.HORIZONTAL;
		gbc_partenza2.gridx = 9;
		gbc_partenza2.gridy = 2;
		frame.getContentPane().add(partenza2, gbc_partenza2);
		
		final JComboBox arrivo1 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_arrivo1 = new GridBagConstraints();
		gbc_arrivo1.insets = new Insets(0, 0, 5, 5);
		gbc_arrivo1.fill = GridBagConstraints.HORIZONTAL;
		gbc_arrivo1.gridx = 1;
		gbc_arrivo1.gridy = 3;
		frame.getContentPane().add(arrivo1, gbc_arrivo1);

		GridBagConstraints gbc_btnCalcola = new GridBagConstraints();
		gbc_btnCalcola.gridwidth = 2;
		gbc_btnCalcola.insets = new Insets(0, 0, 5, 5);
		gbc_btnCalcola.gridx = 4;
		gbc_btnCalcola.gridy = 3;
		frame.getContentPane().add(btnCalcola, gbc_btnCalcola);
		
		final JComboBox arrivo2 = new JComboBox(Controller.listaPercorso().toArray());
		GridBagConstraints gbc_arrivo2 = new GridBagConstraints();
		gbc_arrivo2.insets = new Insets(0, 0, 5, 5);
		gbc_arrivo2.fill = GridBagConstraints.HORIZONTAL;
		gbc_arrivo2.gridx = 9;
		gbc_arrivo2.gridy = 3;
		frame.getContentPane().add(arrivo2, gbc_arrivo2);
		
		JLabel lblPercorso = new JLabel("Percorso 1:");
		GridBagConstraints gbc_lblPercorso = new GridBagConstraints();
		gbc_lblPercorso.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercorso.gridx = 1;
		gbc_lblPercorso.gridy = 5;
		frame.getContentPane().add(lblPercorso, gbc_lblPercorso);
		
		JLabel lblParteComune = new JLabel("Parte comune:");
		GridBagConstraints gbc_lblParteComune = new GridBagConstraints();
		gbc_lblParteComune.insets = new Insets(0, 0, 5, 5);
		gbc_lblParteComune.gridx = 5;
		gbc_lblParteComune.gridy = 5;
		frame.getContentPane().add(lblParteComune, gbc_lblParteComune);
		
		JLabel lblPercorso_1 = new JLabel("Percorso 2:");
		GridBagConstraints gbc_lblPercorso_1 = new GridBagConstraints();
		gbc_lblPercorso_1.insets = new Insets(0, 0, 5, 5);
		gbc_lblPercorso_1.gridx = 9;
		gbc_lblPercorso_1.gridy = 5;
		frame.getContentPane().add(lblPercorso_1, gbc_lblPercorso_1);
		
		final JTextArea text1 = new JTextArea();
		text1.setLineWrap(true);
		GridBagConstraints gbc_text1 = new GridBagConstraints();
		gbc_text1.insets = new Insets(0, 0, 0, 5);
		gbc_text1.fill = GridBagConstraints.BOTH;
		gbc_text1.gridx = 1;
		gbc_text1.gridy = 7;
		frame.getContentPane().add(text1, gbc_text1);
		
		final JTextArea text3 = new JTextArea();
		text3.setLineWrap(true);
		GridBagConstraints gbc_text3 = new GridBagConstraints();
		gbc_text3.insets = new Insets(0, 0, 0, 5);
		gbc_text3.fill = GridBagConstraints.BOTH;
		gbc_text3.gridx = 5;
		gbc_text3.gridy = 7;
		frame.getContentPane().add(text3, gbc_text3);
		
		final JTextArea text2 = new JTextArea();
		text2.setLineWrap(true);
		GridBagConstraints gbc_text2 = new GridBagConstraints();
		gbc_text2.insets = new Insets(0, 0, 0, 5);
		gbc_text2.fill = GridBagConstraints.BOTH;
		gbc_text2.gridx = 9;
		gbc_text2.gridy = 7;
		frame.getContentPane().add(text2, gbc_text2);
		
		btnCalcola.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String a = (String)partenza1.getSelectedItem();
				String b = (String)arrivo1.getSelectedItem();
				String c = (String)partenza2.getSelectedItem();
				String d = (String)arrivo2.getSelectedItem();
				text1.setText((Controller.camminoMinimo(a, b)).toString());
				text2.setText((Controller.camminoMinimo(c, d)).toString());
				text3.setText(Controller.perCom(a, b, c, d).toString());
			}
		});

	}

}
